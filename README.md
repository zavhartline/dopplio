# Dopplio (A.K.A. Vinegar "Vroom Vroom" Dopplio Library)

This project intends to give CreateYourFrisk's NewAudio system a little bit of a boost by adding the ability to have sound sources and observers, which are subject to distance changing volume, the doppler effect, and stereophonic sound.

---

## Setup Instructions:

**1.)** Place Dopplio.lua in the Lua/Libraries/ folder.

**2.)** At the top of the file you want to use the library with, run 
```lua
local dopplio = require 'Libraries/Dopplio'
```
Feel free to change the file paths however you please, so long as they remain consistent.

## Basic Usage Instructions:

The entire system is based around the principle of managing the positions of sound sources relative to the observer. To begin a new set of sources matched to an observer, invoke:
```lua
local instance = dopplio:New(x,y)
```
X and Y in this case are the starting coordinates of your audio's observer. All sound effects will be calculated relative to the observer's position in the plane. An audio observer is useless without anything for it to listen to, however. To create a new audio source, invoke one of the following:
```lua
instance:AddSource(channel, x, y, radius, maxVolume, envelope)
instance:AddStereoSource(leftChannel, rightChannel, x, y, radius, maxVolume, envelope)
```
There's a lot to unpack here. First and foremost, the channels in each  refer to NewAudio channels, and therefore are strings which represent an existing audio channel. For Stereo audio, edit the sound to be entirely left ear in the left channel, and entirely right ear in the right channel. X and Y are the source's initial position on the plane. Radius is the maximum distance from which the audio may be heard by the observer. If radius is nil, distance will not affect the sound's volume in any way. This also renders maxVolume irrelevant. maxVolume is a float between 0 and 1 indicating the highest possible volume the source may reach. 

**For advanced users**

Finally, there is the envelope argument. This one will take a little bit more explaining. The doppler effect occurs in the real world due to sound wave crests being closer or farther apart depending on the relative velocity of the source. This is determined by the speed of sound, however as CYF is digital there is no speed of sound. As such, the programmer may pass a function to the envelope argument which represents the pitch shift based off the difference in velocity between the source and observer. By default, the envelope function is `f(deltaVelocity) = 0.001 * deltaVelocity + 1`. If you wish to add a custom doppler envelope, there are a few criteria to keep in mind:

1) It should pass through (0,1)

2) It shouldn't scale past 0 or 3 extremely quickly, as these are the maximum allowable pitch shifts for forward audio within CYF. 

3) It should be a monotonically decreasing odd function.

These are just guidelines for realistic doppler effect envelopes. In the real world, doppler effects are only linear if they're approached from head-on, so this functionality was added for those who wish to replicate that more precisely. 

---

Congratulations! You now have an observer and at least one audio source. Now all you need to do is add the following to the relevant Update() function:
```lua
instance:Update()
```

There are a few additional functions to help you out with tasks such as moving the source/observer:
```lua
instance:MoveSource(channel, newX, newY)
instance:MoveObserver(channel, newX, newY)
```
Both of these functions move the source to newX and newY based in absolute coordinates. For stereo sources, they are indexed by the left channel.

If you wish to remove a source or get one as a table, simply call `instance:RemoveSource(channel)` and `instance:GetSource(channel)` respectively.

---

