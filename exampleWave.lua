--[[
	Created by Zav Hartline
	https://gitlab.com/zavhartline
]]

local Dopplio = require 'Libraries/Dopplio'

local undyneCar = Dopplio:New(Player.absx, Player.absy)

local c1 = 'left'
NewAudio.CreateChannel(c1)
NewAudio.PlayMusic(c1, 'mus_undyneboss_l')
local c2 = 'right'
NewAudio.CreateChannel(c2)
NewAudio.PlayMusic(c2, 'mus_undyneboss_r')

--Create a stereo source with a radius of 500px
--The coordinates are 0,0 because we override them anyway
undyneCar:AddStereoSource(c1,c2,0,0,500)

local sprite = CreateSprite('bullet', 'Top')

function Update()
    --Move the undynecar boolet to the mouse every frame
    sprite.MoveTo(Input.MousePosX, Input.MousePosY)
    undyneCar:MoveSource(c1, Input.MousePosX, Input.MousePosY)
    --Make the observer follow the player every frame
    undyneCar:MoveObserver(Player.absx, Player.absy)
    
    undyneCar:Update()
end
