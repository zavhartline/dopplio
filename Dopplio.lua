local function Class(members)
	members = members or {}
	local mt = {
		__metatable = members;
    	__index     = members;
  	}
  	local function New()
    	return setmetatable({}, mt)
  	end
	members.New = members.New  or New
	return mt
end

Doppler = {}
local Doppler_mt = Class(Doppler)

function Doppler:New(x,y)
	if x == nil or y == nil then
		error('Cannot instantiate audio observer with nil coordinate')
	end
	return setmetatable({x=x, y=y, lastx=x, lasty=y, sources={}}, Doppler_mt)
end

local function DefaultEnvelope(x)
	return -0.001 * x + 1
end

function Doppler:AddSource(c,x,y,sphere, maxVol, basePitch, envelope)
	if x == nil or y == nil or c == nil then
		error('Attempt to instantiate audio source with nil values')
	end
	if envelope == nil then envelope = DefaultEnvelope end
	if maxVol == nil then
		maxVol = 1
	end
	if basePitch == nil then
		basePitch = 1
	end
	self.sources[c] = {	x=x, y=y,envelope=envelope, 
						sphere=sphere, maxVol=maxVol, 
						basePitch=basePitch, lastx=x,lasty=y, 
						lastdist=nil, isStereo = false}
	return self.sources[c]
end

function Doppler:AddStereoSource(l,r,x,y,sphere, maxVol, basePitch, envelope)
	if x == nil or y == nil or l == nil or r == nil then
		error('Attempt to instantiate audio source with nil values')
	end
	if envelope == nil then envelope = DefaultEnvelope end
	if maxVol == nil then
		maxVol = 1
	end
	if basePitch == nil then
		basePitch = 1
	end
	self.sources[l] = {	left = l, right = r, x=x, y=y,
						envelope=envelope, sphere=sphere, 
						maxVol=maxVol, basePitch=basePitch, 
						lastx=x,lasty=y, lastdist=nil, isStereo = true}
	return self.sources[l]
end

function Doppler:RemoveSource(c)
	self.sources[c] = nil
end

function Doppler:GetSource(c)
	return self.sources[c]
end

function Doppler:MoveSource(c, newX, newY)
	self.sources[c].x = newX
	self.sources[c].y = newY
end

function Doppler:MoveObserver(newX, newY)
	self.x = newX
	self.y = newY
end

function Doppler:Update()
	for c, s in pairs(self.sources) do
		local dist = math.sqrt(math.pow(self.x-s.x, 2) + math.pow(self.y-s.y,2))
		--Control volume
		if s.sphere ~= nil then
			local vol = s.maxVol-dist / s.sphere
			if vol > s.maxVol then vol = s.maxVol end
			if s.isStereo then
				local pd = 1-math.abs(self.x-s.x) / s.sphere
				local pvol = pd * vol
				--DEBUG(string.format("%0.4f / %0.4f / %0.4f", pd, pvol, vol))
				if vol <= 0 then pvol = 0 end
				if s.x < self.x then
					NewAudio.SetVolume(s.left, vol)
					NewAudio.SetVolume(s.right, pvol)
				else
					NewAudio.SetVolume(s.left, pvol)
					NewAudio.SetVolume(s.right, vol)
				end
			else
				NewAudio.SetVolume(c, vol)
			end
		end
		--Control pitch
		if s.lastdist == nil then

		else
			local dz = dist-s.lastdist
			local df = s.envelope(dz) * s.basePitch
			if df > 3 then df = 3 end
			if df <= 0 then df = 0.01 end
			if s.isStereo then
				NewAudio.SetPitch(s.left, df)
				NewAudio.SetPitch(s.right, df)
			else
				NewAudio.SetPitch(c, df)
			end
		end
		s.lastdist = math.sqrt(math.pow(self.x-s.lastx, 2) + math.pow(self.y-s.lasty,2))
		s.lastx = s.x
		s.lasty = s.y
	end
	self.lastx = self.x
	self.lasty = self.y
end

return Doppler
